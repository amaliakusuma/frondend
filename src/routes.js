import DashView from './components/Dash.vue'
import LoginView from './components/Login.vue'
import NotFoundView from './components/404.vue'

// Import Views - Dash
import DashboardView from './components/views/Dashboard.vue'
import TablesView from './components/views/Tables.vue'
import TabelKec from './components/views/Kecamatan.vue'
import Tahun from './components/views/Tahun.vue'
import DataTB from './components/views/DataTB.vue'
import TingkatKerawanan from './components/views/TingkatKerawanan.vue'
import FasilitasKesehatan from './components/views/FasilitasKesehatan.vue'
import InfoTB from './components/views/InfoTB.vue'
import TasksView from './components/views/Tasks.vue'
import SettingView from './components/views/Setting.vue'
import AccessView from './components/views/Access.vue'
import ServerView from './components/views/Server.vue'
import ReposView from './components/views/Repos.vue'

// Routes
const routes = [
  {
    path: '/login',
    component: LoginView
  },
  {
    path: '/',
    component: DashView,
    children: [
      {
        path: 'dashboard',
        alias: '',
        component: DashboardView,
        name: 'Dashboard',
        meta: {description: 'Overview of environment'}
      }, {
        path: 'tables',
        component: TablesView,
        name: 'Tables',
        meta: {description: 'Simple and advance table in CoPilot'}
      }, {
        path: 'tabelkec',
        component: TabelKec,
        name: 'TabelKecamatan',
        meta: {description: 'Simple and advance table in CoPilot'}
      }, {
        path: 'tahun',
        component: Tahun,
        name: 'Tahun',
        meta: {description: 'Simple and advance table in CoPilot'}
      }, {
        path: 'dataTB',
        component: DataTB,
        name: 'DataTB',
        meta: {description: 'Simple and advance table in CoPilot'}
      }, {
        path: 'tingkatkerawanan',
        component: TingkatKerawanan,
        name: 'TingkatKerawanan',
        meta: {description: 'Simple and advance table in CoPilot'}
      }, {
        path: 'faskes',
        component: FasilitasKesehatan,
        name: 'FasilitasKesehatan',
        meta: {description: 'Simple and advance table in CoPilot'}
      }, {
        path: 'infoTB',
        component: InfoTB,
        name: 'InfoTB',
        meta: {description: 'Simple and advance table in CoPilot'}
      }, {
        path: 'tasks',
        component: TasksView,
        name: 'Tasks',
        meta: {description: 'Tasks page in the form of a timeline'}
      }, {
        path: 'setting',
        component: SettingView,
        name: 'Settings',
        meta: {description: 'User settings page'}
      }, {
        path: 'access',
        component: AccessView,
        name: 'Access',
        meta: {description: 'Example of using maps'}
      }, {
        path: 'server',
        component: ServerView,
        name: 'Servers',
        meta: {description: 'List of our servers', requiresAuth: true}
      }, {
        path: 'repos',
        component: ReposView,
        name: 'Repository',
        meta: {description: 'List of popular javascript repos'}
      }
    ]
  }, {
    // not found handler
    path: '*',
    component: NotFoundView
  }
]

export default routes
